# RFC 821 - SIMPLE MAIL TRANSFER PROTOCOL

RFC 821 outlines the Simple Mail Transfer Protocol and it's functionality, written by Jonathan B. Postel.
SMTP is only requires a reliable ordered data stream channel and is therefor capable of being used over many transport services.

# SMTP Model
Communication between two SMTP's is established after a user's mail request. The sender-SMTP then sends the needed commands & data to the reciever-SMTP, the reciever-SMTP replies to each of the commands with a service code depending on what happend.

# SMTP Commands 
SMTP Commands are shortened 4-letter versions of their functions ex. the HELLO command is shortened to HELO.
On top of them being shortened they are also case in-sensitive meaning the the HELLO command can be written as: hello , HeLlO , HELLo , etc.
They are terminated with a space (<SP>) if parameters follow or a line-feed (<CRLF>) if not.
The commands described as of RFC 821 section 4.1 are:

|Function |Command |
|---------|--------|
|Hello|HELO|
|Mail|MAIL|
|Recipient|RCPT|
|Data|DATA|
|Send|SEND|
|Send or Mail|SOML|
|Send and Mail|SAML|
|Reset|RSET|
|Verify|VRFY|
|Expand|EXPN|
|Help|HELP|
|Noop|NOOP|
|Quit|QUIT|
|Turn|TURN|

Further description of the commands can be found at section 4.1


# Reciver Reply
Whenever a command is recieved by a SMTP-server it sends a reply/service-code in the range from 1XX - 5XX each range having it's own meaning.

|Range|Code|
|-----|----|
|1XX|Error|
|2XX|Success|
|3XX|Error|
|4XX|Failure|
|5XX|Failure|

State diagrams about the reply-codes and when/how they get sendt is available in section 4.4 in the RFC, while explanaition of the reply-codes are available in section 4.2.


# Links
RFC: [https://tools.ietf.org/html/rfc821](https://tools.ietf.org/html/rfc821)
