# Following guide:
[https://starbeamrainbowlabs.com/blog/article.php?article=posts%2F237-WebDav-Nginx-Setup.html](https://starbeamrainbowlabs.com/blog/article.php?article=posts%2F237-WebDav-Nginx-Setup.html)

# Errors:

## Template
<b>Issue</b>

<b>Solution</b>


## Nautilus 23.34
<b>Issue</b>
Not a webdav enabled share
405 - not allowed

<b>Solution</b>
nginx only serves standard page on loopback, add chosen name to "hosts" file


## Nautilus 23.47
<b>Issue</b>
Authorization required / authentication needed
Blank - blank doesn't work

<b>Attemped Solution</b>
Removing the lines for basic auth, this gave a 404 as it started looking for a new index.html

<b>Solution</b>
Add a .passwords.list file according to the guide


## Nautilus 00.39
<b>Issue</b>
Could not find an enclosing directory
`dav_ext stat failed on '/mnt/stored/' (2: No such file or directory)`

<b>Solution</b>
created folder /mnt/stored/