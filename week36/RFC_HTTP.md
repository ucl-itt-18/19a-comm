# RFC - HTTP

### RFC 2324 HTCPCP/1.0

Very nice protocol :y:

### RFC 7168 HTCPCP-TEA

Very nice extension :v: I prefer this one!

### RFC 1945 HTTP/1.0

This RFC is a reflection on the usage of HTTP for the past 6 years. It covers alot about all aspects of HTTP.

### RFC 2616 HTTP/1.1

This RFC is about an update to RFC 2068 which also is about HTTP/1.1. It covers alot about all aspects of HTTP.

### RFC 4918 WebDAV

This RFC describes an extension to the HTTP/1.1 protocol which allows clients to perform remote Web content authoring operations.

### RFC 8615 Well-known URIs

This RFC is a description of well-known URIs and the association call IANA who takes care of a central list will all wall known URIs. Apparently not really used alot.


# HTTP
HTTP is a protocol that works on the application layer of the OSI model. HTTP has been used since 1990 but was first writen in the RFC by 1996 as a reflection [RFC 1945](https://tools.ietf.org/html/rfc1945).
HTTP is used by the World Wide Web and defines how messages are formated and transmitted. It also defines what actions are requested by the Servers when recieved. 
These actions are called methods. The most common headers are GET, SET, PUT, HEAD. Along with the method are some request/respond/general headers. These headers contain some meta data about the packet like: 
host, method, path, version. HTTP also contains a body with data of whatever was requested. The latest version of HTTP is HTTP/2.0 [RFC 7540](https://tools.ietf.org/html/rfc7540) and was developed by google.
This newer version is faster and backwardscompatible with HTTP/1.1. HTTP/3.0 is in development and will be using UDP instead of TCP.
