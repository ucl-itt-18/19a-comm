# HTTP/1.1 RFC 2616

[https://tools.ietf.org/html/rfc2616](https://tools.ietf.org/html/rfc2616)

## Terminology

### connection
"A transport layer virtual circuit established between two programs for the purpose of communication."

### message
Structured sequence of 8-bits according to the defined syntax.

### request & response
A type of HTTP message

### resource
URI-identifiable network data object (data-stream/package) & [service](https://www.sdxcentral.com/term/network-service/)

### entity
The payload of a request or response.

### respresentation
Entity included whith a response that is subject to negotiation

### content negotiation
Mechanism for selecting the right representation when servicing a request.
There exists Server-driven and Agent-driven negotiation.

### variant
Different representation of a recsource.

### client
Program establishing connections to send request.

### user agant
Client initiating request. (Browser, webcrawler, etc.)

### server
Application program accepting connections, services requests by sending back responses.

### origin server
Server holding/creating the given recource

### proxy
Program making request on behalf of other clients, acts like both server and client. Possibly handles requests internally or passes them on (translated if needed).

### gateway
Server acting as a origin server but actually only forwards the messages when recieving them.

### tunnel
Program acting as blind relay between two connections.

### cache
"A program's local store of response messages and the subsystem that controls its message storage, retrieval, and deletion.

### cacheable
Response if cachable if caches may store a copy of the response message for answering future request.

### first-hand
A response is first-hand if it travels directly from the origin server without any unnecessary delay, or it has just been checked directly with the origin server.

### explicit expiration time
The time at which the origin server says that a entity should't be returned by cache without further validation.

### heuristic expiration time
Expiration time assigned by cache if no explicit is available.

### age
Time since responce sent or validated with origin server.

### freshness lifetime
The amount of time between generation of response and its expiration time.

### fresh
State of response if it's in its freshness lifetime

### stale
State of response if it's past is freshness lifetime.

### semantically transparent
Cache behavior
When a connecting client recieves exaclty the same response as if the origin server had handled it.

### validator
Protocol element (e.g., last-modified, entity tag) used to validate if cache entry is equal to an entity

### upstream/downstream
Flow of message
They always flow from up- to downstream


### inbound/outbound
response paths for messages
inbound - travelling towards origin server
outbound - traveling toward the user agent



## Messages
Combination of Client-request & Server-response. Ending with a linefeed.


### Header fields: 
`<Header-field-name>:<SP><field-value><CRLF>`<br>
Field-names are case-insensitive
Field-value can be preceeded by any amount Linear White Space (LWS), single SP prefered.
Extra lines need be preceeded with <SP> or <HP>

`message-header = field-name ":" [ field-value ]
field-name     = token
field-value    = *( field-content | LWS )
field-content  = <the OCTETs making up the field-value
                and consisting of either *TEXT or combinations
                of token, separators, and quoted-string>
`

Leading / trailing LWS may be removed. LWS between field-content MAY be replaced with single SP.

Best practice to send general header-files first, then request-/response-header and ending with entity-header fields.

Multiple message header fields with same name can only exist if they are a comma-seperated list as they must be "combinable" (Append the next value to the previous one). Therefor the order is important and proxies can't swap the order of messages.

### Message Body
Doesn't have to be there
Only differes from entity-body when transfer-encoded (specified by the "Transfer-encoding" header field).
[Content-Transfer-Encoding Header Field w3.org](https://www.w3.org/Protocols/rfc1341/5_Content-Transfer-Encoding.html)

`       message-body = entity-body
                    | <entity-body encoded as per Transfer-Encoding>`


Transfer-Encoding is a property of the message and may therefor be removed at any time.
Message-body occurence is signaled by the Content-length or Transfer-Encoding header.

## Status/Return Codes

### Informational 1xx
Only Status-Line and optional headers. Serves a [provisional](https://www.merriam-webster.com/dictionary/provisional) response.
NOT for HTTP/1.0 as it's not defined for it.

### Successful 2xx
Indicates that the request has been successfully recieved, understood and accepted.

### Redirection 3xx
Further action needed by the user agent.
User-agent can take action without user interaction if method is GET or HEAD.

### Client Errror 4xx
Client has/had made an error. 
