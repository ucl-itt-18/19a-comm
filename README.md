# 19A-Comm

This repository is a common "dumping ground" for student exercises.  
The folders correspond to their respective work week and their weekly topic.

| Teacher |  Week | Content |
| :---: | :---: | :--- |
|  MON | 35 | Introduction, tools and online resources |
|  MON | 36 | HTTP(S) and webdav |
|  MON | 37 | Telnet, SSH |
|  MON | 38 | DNS, DNS over TLS, DNS over HTTPS |
|  MON | 39 | (S)SMTP, IMAP(S), POP3(S), SPF, DKIM, DMARC |
|  MON | 40 | VPN, IPSEC, OpenVPN, wireguard |
|  MON | 41 | Exams |
|  MON | 42 | Vacation |